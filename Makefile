#
# Makefile
# Author: Craig Liebmann
#

CC = g++
CCFLAGS = 
BOOST_HEADERS = /usr/local/boost_1_54_0
BOOST_LIBS = /usr/local/boost_1_54_0/stage/lib/

OBJS = Platform.o Communication.o Order.o Message.o Parser.o Command.o OrderBook.o cms.o Server.o Connection.o
# CmdQueue.o

all: cms

clean:
	rm *.o

cms: $(OBJS)
	$(CC) $(CCFLAGS) $(OBJS) -o cms -L $(BOOST_LIBS) -l boost_system -l boost_thread -l boost_regex

cms.o: cms.cpp Platform.hpp
	$(CC) $(CCFLAGS) -c cms.cpp

# CmdQueue.hpp
Server.o: Server.cpp Server.hpp Connection.hpp Platform.hpp MyDebug.hpp
	$(CC) $(CCFLAGS) -c Server.cpp

# CmdQueue.hpp
Connection.o: Connection.cpp Connection.hpp Platform.hpp Message.hpp Command.hpp Parser.hpp MyDebug.hpp
	$(CC) $(CCFLAGS) -c Connection.cpp

# CmdQueue.o: CmdQueue.cpp CmdQueue.hpp Command.hpp Connection.hpp
# 	$(CC) $(CCFLAGS) -c CmdQueue.cpp -I $(BOOST_HEADERS)

Platform.o: Platform.cpp Platform.hpp Server.hpp Communication.hpp Message.hpp Parser.hpp Command.hpp OrderBook.hpp MyDebug.hpp
	$(CC) $(CCFLAGS) -c Platform.cpp

OrderBook.o: OrderBook.cpp OrderBook.hpp Order.hpp Command.hpp MyDebug.hpp
	$(CC) $(CCFLAGS) -c OrderBook.cpp

Command.o: Command.cpp Command.hpp Message.hpp OrderBook.hpp MyDebug.hpp
	$(CC) $(CCFLAGS) -c Command.cpp

Parser.o: Parser.cpp Parser.hpp Command.hpp MyDebug.hpp
	$(CC) $(CCFLAGS) -c Parser.cpp

Message.o: Message.cpp Message.hpp Order.hpp MyDebug.hpp
	$(CC) $(CCFLAGS) -c Message.cpp

Order.o: Order.cpp Order.hpp Command.hpp MyDebug.hpp
	$(CC) $(CCFLAGS) -c Order.cpp

Communication.o: Communication.cpp Communication.hpp MyDebug.hpp
	$(CC) $(CCFLAGS) -c Communication.cpp -I $(BOOST_HEADERS) 