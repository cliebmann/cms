//
// Platform.cpp
// Author: Craig Liebmann
//

#include "Platform.hpp"
#include "Communication.hpp"
#include "Message.hpp"
#include "Command.hpp"
#include "Parser.hpp"
#include "Connection.hpp"
#include "MyDebug.hpp"
// #include "Server.hpp"

#include <string>
#include <set>
#include <iostream>

#include <boost/asio.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>

using namespace std;
using boost::asio::ip::tcp;



Platform::Platform() {
    DEBUG_STDOUT("Platform::Platform\n");
    ob_ = new OrderBook();
}

Platform::~Platform() {
    DEBUG_STDOUT("Platform::~Platform\n");
    delete ob_;
}

//-----------------------------------------------------------------------------

Blocking_Platform::Blocking_Platform() {
    DEBUG_STDOUT("Blocking_Platform::Blocking_Platform\n");
}

Blocking_Platform::~Blocking_Platform() {
    DEBUG_STDOUT("Blocking_Platform::~Blocking_Platform\n");
}

void Blocking_Platform::run() {
    try {

        string line;
        for (;;) {
            line = comm_->read();
            // if (line.empty()) break;

            Input_Message* im = new Input_Message(line);
            if (!im->isValid()) { 
                comm_->write(ERROR(INVALID_MESSAGE).to_str());
                delete im;
                continue; 
            }

            queue<string> tokens = im->tokenize();
            delete im;

            Parser p;
            Command* cmd = p.parse(tokens);
            if (!cmd) { 
                comm_->write(ERROR(INVALID_MESSAGE).to_str());
                // cmd is NULL so don't try to delete
                continue; 
            }

            Output_Message* om = cmd->execute(*ob_);                
            delete cmd;

            comm_->write(om->to_str());

            delete om;
        }
    }
    catch (std::exception& e)
    {
        cerr << "EXCEPTION: " << e.what() << endl;
    }
}

//-----------------------------------------------------------------------------

Base_Platform::Base_Platform() {
    DEBUG_STDOUT("Base_Platform::Base_Platform\n");
    comm_ = new Std_Comm();
    //ob_ = new OrderBook();
}

Base_Platform::~Base_Platform() {
    DEBUG_STDOUT("Base_Platform::~Base_Platform\n");
    delete comm_;
    //delete ob_;
}

//-----------------------------------------------------------------------------

TCP_Platform::TCP_Platform(boost::asio::io_service& io_service, short port) 
  : port_(port) {
    DEBUG_STDOUT("TCP_Platform::TCP_Platform\n");
    comm_ = new TCP_Comm(io_service, port_);
    //ob_ = new OrderBook();
}

TCP_Platform::~TCP_Platform() {
    DEBUG_STDOUT("TCP_Platform::TCP_Platform\n");
    delete comm_;    
    //delete ob_;
}

//-----------------------------------------------------------------------------


Async_Platform::Async_Platform(boost::asio::io_service& io_service, short port) 
  : io_service_(io_service), 
    acceptor_(io_service, tcp::endpoint(tcp::v4(), port)),
    socket_(io_service)
{
    DEBUG_STDOUT("Async_Platform::Async_Platform\n");
    //ob_ = new OrderBook();
    start_accept();
}

Async_Platform::~Async_Platform() {
    DEBUG_STDOUT("Async_Platform::~Async_Platform\n");
    //delete ob_;
}

void Async_Platform::run() {
    io_service_.run();
}

void Async_Platform::start_accept() {
    acceptor_.async_accept(socket_,
        boost::bind(&Async_Platform::handle_accept, this,
            boost::asio::placeholders::error));
}

void Async_Platform::handle_accept(const boost::system::error_code& error) {
    DEBUG_STDOUT("handle_accept\n");
    if (!error) {
        start_session();
    }
    else {
        DEBUG_STDOUT("handle_accept: error\n");
    }
    // just return -- only one session for now
}

void Async_Platform::start_session() {
    socket_.async_read_some(boost::asio::buffer(data_, max_length),
        boost::bind(&Async_Platform::handle_read, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void Async_Platform::handle_read(const boost::system::error_code& error, 
    size_t bytes_transferred) {
    DEBUG_STDOUT("handle_read\n");
    if (!error) {
        string msg = execute(bytes_transferred);
        stringstream ss;
        ss << msg << endl;
        string s = ss.str();

        boost::asio::async_write(socket_,
            boost::asio::buffer(s, s.size()),
            boost::bind(&Async_Platform::handle_write, this,
                boost::asio::placeholders::error));
    }
    else {
        DEBUG_STDOUT("handle_read: error\n");
    }
    // just return
}

void Async_Platform::handle_write(const boost::system::error_code& error) {
    DEBUG_STDOUT("handle_write\n");
    if (!error) {
        socket_.async_read_some(boost::asio::buffer(data_, max_length),
            boost::bind(&Async_Platform::handle_read, this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred));
    }
    else {
        DEBUG_STDOUT("handle_write: error\n");
    }
    // just return
}

// is this asynchronous?
string Async_Platform::execute(size_t bytes_transferred) {
    try {
        string line = string(data_, bytes_transferred);

        Input_Message* im = new Input_Message(line);
        if (!im->isValid()) {
            delete im;
            return ERROR(INVALID_MESSAGE).to_str();
        }

        queue<string> tokens = im->tokenize();
        delete im;

        Parser p;
        Command* cmd = p.parse(tokens);
        if (!cmd) {
            return ERROR(INVALID_MESSAGE).to_str();
        }

        Output_Message* om = cmd->execute(*ob_);
        delete cmd;

        string out = om->to_str();
        delete om;
        return out;
    }    
    catch (std::exception& e)
    {
        cerr << "EXCEPTION: " << e.what() << endl;
    }
}

//-----------------------------------------------------------------------------

Multi_Async_Platform::Multi_Async_Platform(boost::asio::io_service& io_service, 
    const std::string& address, const std::string& port) 
  : s_(io_service, address, port, this)
{
    DEBUG_STDOUT("Multi_Async_Platform::Multi_Async_Platform\n");
}


Multi_Async_Platform::~Multi_Async_Platform() {
    DEBUG_STDOUT("Multi_Async_Platform::~Multi_Async_Platform\n");
}

void Multi_Async_Platform::run() {
    s_.run();

    // cout << "create threads" << endl;
    // boost::thread t1(&Multi_Async_Platform::execute, this);
    // boost::thread t2(&Multi_Async_Platform::run_server, this);
    
    // t1.join();
    // t2.join();

}

// void Multi_Async_Platform::run_server() {
//     s_.run();
// }

// void Multi_Async_Platform::execute() {
//     cout << "Multi_Async_Platform::execute" << endl;
//     while (true) { // probably bad
//         boost::this_thread::interruption_point(); // better way?

//         // if (!queue_.empty()) continue;

//         struct cmd_conn_pair ccp = queue_.pop();
//         Output_Message* om = ccp.cmd_p->execute(*ob_);
//         delete ccp.cmd_p;

//         //cout << ccp.conn_sp->socket().is_open() << endl;
//         ccp.conn_sp->write(om->to_str());
//         delete om;
//     }
// }









