//
// Platform.hpp
// Author: Craig Liebmann
//

#ifndef PLATFORM_HPP
#define PLATFORM_HPP

#include "OrderBook.hpp"
#include "Communication.hpp"
#include "Connection.hpp"
#include "Server.hpp"
//#include "CmdQueue.hpp"
#include <string>
#include <set>
#include <boost/asio.hpp>
#include <boost/thread/mutex.hpp>

class Server;
//class CmdQueue;

class Platform {
public:
    Platform();
    virtual ~Platform() = 0;
    virtual void run() = 0;

    OrderBook* ob_;
};

class Blocking_Platform : public Platform{
public:
    Blocking_Platform();
    virtual ~Blocking_Platform() = 0;
    void run();

    Communication* comm_;
};

//-----------------------------------------------------------------------------

class Base_Platform : public Blocking_Platform {
public:
    Base_Platform();
    ~Base_Platform();
};

//-----------------------------------------------------------------------------

class TCP_Platform : public Blocking_Platform {
public:
    TCP_Platform(boost::asio::io_service& io_service, short port);
    ~TCP_Platform();

private:
    unsigned short port_;
};

//-----------------------------------------------------------------------------

class Async_Platform : public Platform {
public:
    Async_Platform(boost::asio::io_service& io_service, short port);
    ~Async_Platform();
    void run();

private:
    void start_accept();
    void handle_accept(const boost::system::error_code& error);

    void start_session();

    void handle_read(const boost::system::error_code& error, size_t bytes_transferred);
    void handle_write(const boost::system::error_code& error);

    std::string execute(size_t bytes_transferred);

    boost::asio::io_service& io_service_;
    boost::asio::ip::tcp::acceptor acceptor_;
    boost::asio::ip::tcp::socket socket_;
    enum { max_length = 1024 };
    char data_[max_length];
};

//-----------------------------------------------------------------------------

class Multi_Async_Platform : public Platform {
public:
    Multi_Async_Platform(boost::asio::io_service& io_service, 
        const std::string& address, const std::string& port);
    ~Multi_Async_Platform();
    void run();

    boost::mutex mutex_;

private:
    void execute();
    void run_server();

    Server s_;
    //CmdQueue queue_;
};

#endif // PLATFORM_HPP