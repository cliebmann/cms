//
// Command.hpp
// Author: Craig Liebmann
//
// return Output_Message* or Output_Message& ???
//

#ifndef COMMAND_HPP
#define COMMAND_HPP

//#include "Message.hpp"
//#include "OrderBook.hpp"
#include <string>
#include <queue>
#include <boost/shared_ptr.hpp>

class Output_Message;
class OrderBook;

//-----------------------------------------------------------------------------

struct aggress_pair {
    int order_id;
    int amount;
};

//-----------------------------------------------------------------------------

class Command {
public:
    Command(std::string dealer_id);
    virtual ~Command() = 0;

    // caller owns the memory of the Output_Message pointed to 
    virtual Output_Message* execute(OrderBook& ob) = 0;

protected:
    std::string dealer_id_;
};

//-----------------------------------------------------------------------------

typedef boost::shared_ptr<Command> command_ptr;

//-----------------------------------------------------------------------------

class POST : public Command {
public:
    POST(std::string dealer_id, 
         std::string side,
         std::string commodity,
         int amount, 
         double price);
    ~POST();

    // inline Accessors
    std::string getDealerId() const { return dealer_id_; }
    std::string getSide() const { return side_; }
    std::string getCommodity() const { return commodity_; }
    int getAmount() const { return amount_; }
    double getPrice() const { return price_; }

    Output_Message* execute(OrderBook& ob);

private:
    std::string side_;
    std::string commodity_;
    int amount_;
    double price_;
};

//-----------------------------------------------------------------------------

class REVOKE : public Command {
public:
    REVOKE(std::string dealer_id,
           int order_id);
    ~REVOKE();

    Output_Message* execute(OrderBook& ob);

private:
    int order_id_;
};

//-----------------------------------------------------------------------------

class CHECK : public Command {
public:
    CHECK(std::string dealer_id,
           int order_id);
    ~CHECK();

    Output_Message* execute(OrderBook& ob);

private:
    int order_id_;
};

//-----------------------------------------------------------------------------

class LIST : public Command {
public:
    // optional args?
    LIST(std::string dealer_id);
    LIST(std::string dealer_id,
         std::string commodity);
    LIST(std::string dealer_id,
         std::string commodity,
         std::string other_dealer_id);
    ~LIST();

    Output_Message* execute(OrderBook& ob);

private:
    std::string commodity_;
    std::string other_dealer_id_;
};

//-----------------------------------------------------------------------------

class AGGRESS : public Command {
public:
    // deal with more than one?
    AGGRESS(std::string dealer_id,
            std::queue<aggress_pair> aggress_pairs);
    ~AGGRESS();

    Output_Message* execute(OrderBook& ob);

private:
    std::queue<aggress_pair> aggress_pairs_;
};

//-----------------------------------------------------------------------------

#endif // COMMAND_HPP




