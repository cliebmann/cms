//
// Communication.hpp
// Author: Craig Liebmann
//

#ifndef COMMUNICATION_HPP
#define COMMUNICATION_HPP

#include <string>
#include <boost/asio.hpp>
//#include <boost/smart_ptr.hpp>


class Communication {
public:
    Communication();
    virtual ~Communication() = 0;
    virtual std::string read() = 0;
    virtual void write(std::string msg) = 0;
};

//-----------------------------------------------------------------------------

class Std_Comm : public Communication {
public:
    Std_Comm();
    ~Std_Comm();
    std::string read();
    void write(std::string msg);
};

//-----------------------------------------------------------------------------

typedef boost::shared_ptr<boost::asio::ip::tcp::socket> socket_ptr;

class TCP_Comm : public Communication {
public:
    TCP_Comm(boost::asio::io_service& io_service, unsigned short port);
    ~TCP_Comm();
    std::string read();
    void write(std::string msg);

private:
    unsigned short port_;
    //boost::asio::ip::tcp::socket* socketp_;
    socket_ptr socketp_;
};


#endif // COMMUNICATION_HPP
