//
// Communication.cpp
// Author: Craig Liebmann
//

#include "Communication.hpp"
#include "MyDebug.hpp"
#include <string>
#include <iostream>

#include <boost/asio.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/bind.hpp>

using namespace std;
using boost::asio::ip::tcp;

const int max_length = 1024;

Communication::Communication() {
    DEBUG_STDOUT("Communication::Communication\n");
}

Communication::~Communication() {
    DEBUG_STDOUT("Communication::~Communication\n");
}

//-----------------------------------------------------------------------------

Std_Comm::Std_Comm() {
    DEBUG_STDOUT("Std_Comm::Std_Comm\n");
}

Std_Comm::~Std_Comm() {
    DEBUG_STDOUT("Std_Comm::~Std_Comm\n");
}

string Std_Comm::read() {
    string line;
    getline(cin, line);

    return line;
}

void Std_Comm::write(std::string msg) {
    cout << msg << endl;
}

//-----------------------------------------------------------------------------

// typedef boost::shared_ptr<boost::asio::ip::tcp::socket> socket_ptr;

TCP_Comm::TCP_Comm(boost::asio::io_service& io_service, unsigned short port) 
  : port_(port)
{
    DEBUG_STDOUT("TCP_Comm::TCP_Comm\n");
    //boost::asio::io_service io_service;
    tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port_));
    /* tcp::socket* */
    socketp_ = socket_ptr(new tcp::socket(io_service));
    a.accept(*socketp_);
}

TCP_Comm::~TCP_Comm() {
    DEBUG_STDOUT("TCP_Comm::~TCP_Comm\n");
    //delete socketp_;
}

string TCP_Comm::read() {
    char data[max_length];


    boost::system::error_code error;
    size_t length = socketp_->read_some(boost::asio::buffer(data), error);

    if (error == boost::asio::error::eof) return "";
    else if (error) {
        throw boost::system::system_error(error);
    }

    return string(data, length);
}

void TCP_Comm::write(string msg) {
    

    stringstream ss;
    ss << msg << endl;
    string s = ss.str();

    // char* data = new char[msg.size() + 1];
    // data[s.size()] = 0;
    // memcpy(data, s.c_str(), s.size());

    boost::asio::write(*socketp_, boost::asio::buffer(s, s.size()));
    //socketp_->write_some(boost::asio::buffer(s, s.size()));
}






