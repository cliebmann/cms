//
// Server.hpp
// Author: Craig Liebmann
//

#ifndef SERVER_HPP
#define SERVER_HPP

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
// #include "Connection.hpp"
// #include "CmdQueue.hpp"
// #include "Platform.hpp"
#include <string>
#include <set>

// class CmdQueue;
class Multi_Async_Platform;
class Connection;

class Server {
public:
    Server(boost::asio::io_service& io_service, const std::string& address, 
        const std::string& port, Multi_Async_Platform* pm);
    ~Server();

    void run();

    void start_connection(boost::shared_ptr<Connection> csp);
    void stop_connection(boost::shared_ptr<Connection> csp);
    
private:
    void start_accept();
    void handle_accept(const boost::system::error_code& error);
    void handle_stop();


    void stop_all_connections();

    boost::asio::io_service& io_service_;
    boost::asio::ip::tcp::acceptor acceptor_;

    std::set<boost::shared_ptr<Connection> > connections_;
    boost::shared_ptr<Connection> new_connection_;
    // boost::asio::signal_set signals_;

    Multi_Async_Platform* pm_;

    bool accepted_;
};

#endif // SERVER_HPP