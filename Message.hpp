//
// Message.hpp
// Author: Craig Liebmann
//

#ifndef MESSAGE_HPP
#define MESSAGE_HPP

#include "Order.hpp"
#include <string>
#include <queue>
#include <iostream>




//-----------------------------------------------------------------------------

class Input_Message {
public:
    enum { max_length = 255 };

    Input_Message(std::string line);
    ~Input_Message();

    bool isValid();
    std::queue<std::string> tokenize();

private:
    bool testValidity();

    std::string content_;
    bool valid_;
};

//-----------------------------------------------------------------------------

class Output_Message {
public:
    Output_Message();
    virtual ~Output_Message() = 0;

    virtual std::string to_str() const = 0;

    // doesn't work
    friend std::ostream& operator<<(std::ostream& os, const Output_Message& om);
};



class ORDER_INFO : public Output_Message {
public:
    ORDER_INFO(Order& order);
    ~ORDER_INFO();

    std::string to_str() const;
    std::string to_str_plain() const;

private:
    // copy over members?
    Order order_;

    // int order_id_;
    // std::string dealer_id_;
    // std::string side_;
    // std::string commodity_;
    // int remaining_amount_;
    // double price_;
};


class ORDER_INFO_LIST : public Output_Message {
public:
    ORDER_INFO_LIST();
    ~ORDER_INFO_LIST();

    void add_order(Order& order);
    std::string to_str() const;

private:
    // vector?
    std::vector<ORDER_INFO> orders_; 
};

enum Action { BOUGHT, SOLD };

class TRADE_REPORT : public Output_Message {
public:
    TRADE_REPORT(Action act, int amount, std::string commodity,
                 double price, std::string dealer_id);
    ~TRADE_REPORT();

    std::string to_str() const;
    std::string to_str_plain() const;

private:
    std::string action_; 
    int amount_;
    std::string commodity_;
    double price_;
    std::string dealer_id_;
};

class TRADE_REPORT_LIST : public Output_Message {
public:
    TRADE_REPORT_LIST();
    ~TRADE_REPORT_LIST();

    void add_trade_report(TRADE_REPORT& tr);
    std::string to_str() const;

private:
    std::vector<TRADE_REPORT> trades_;
};


class FILLED : public Output_Message {
public:
    FILLED(int order_id);
    ~FILLED();

    std::string to_str() const;

private:
    int order_id_;
};


class REVOKED : public Output_Message {
public:
    REVOKED(int order_id);
    ~REVOKED();

    std::string to_str() const;

private:
    int order_id_;
};


class POST_CONFIRMATION : public Output_Message {
public:
    POST_CONFIRMATION(ORDER_INFO& oi);
    ~POST_CONFIRMATION();

    std::string to_str() const;

private:
    ORDER_INFO oi_;
};


enum Error_Type { UNAUTHORIZED, UNKNOWN_ORDER, UNKNOWN_DEALER,
                  UNKNOWN_COMMODITY, INVALID_MESSAGE };

class ERROR : public Output_Message {
public:
    ERROR(Error_Type et);
    ~ERROR();

    std::string to_str() const;

private:
    enum Error_Type et_;
};





//-----------------------------------------------------------------------------

#endif // MESSAGE_HPP