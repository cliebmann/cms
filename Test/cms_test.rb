#!/usr/bin/env ruby

require 'socket'


def send_to_cms(s, min)
    s.write(min)
    
    mout = ""
    loop do
        tmp = s.readline
        mout << tmp
        break if (tmp.scan(/\d+/)[0].to_i == 0)
    end
    mout
end


def test(s, min, should_be)
    $total += 1
    mout = send_to_cms(s, min)

    puts "Sent: #{min}"
    if mout != should_be
        puts "Received: #{mout}"
        puts "ShouldBe: #{should_be}"
        puts "Test failed"
        $failed += 1
    else
        puts "Test passed"
    end
    puts "----------------------------------------------------------"
end


if __FILE__ == $PROGRAM_NAME
    host = 'localhost'
    port = 2000

    puts "Testing Blocking-TCP-CMS via synchronous client"
    puts "----------------------------------------------------------"

    $total = 0
    $failed = 0

    s = TCPSocket.open(host, port)

    # INVALID_MESSAGE
    test(s, "\n", "0> INVALID_MESSAGE\n")
    test(s, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\n",
         "0> INVALID_MESSAGE\n")
    test(s, "DB POST\n", "0> INVALID_MESSAGE\n")
    test(s, "DB CHECK\n", "0> INVALID_MESSAGE\n")
    test(s, "DB REVOKE\n", "0> INVALID_MESSAGE\n")
    test(s, "DB AGGRESS\n", "0> INVALID_MESSAGE\n")
    test(s, "DB AGGRESS 1\n", "0> INVALID_MESSAGE\n")
    test(s, "DB AGGRESS 1 2 1\n", "0> INVALID_MESSAGE\n")

    test(s, "DB POST BLAH\n", "0> INVALID_MESSAGE\n")
    test(s, "DB POST BUY RICE 1 2 BLAH\n", "0> INVALID_MESSAGE\n")
    test(s, "DB CHECK BLAH\n", "0> INVALID_MESSAGE\n");
    test(s, "DB REVOKE BLAH\n", "0> INVALID_MESSAGE\n");
    test(s, "DB AGGRESS BLAH 1\n", "0> INVALID_MESSAGE\n");
    test(s, "DB AGGRESS 1 BLAH\n", "0> INVALID_MESSAGE\n");


    test(s, "DB POST BUY RICE 1 BLAH\n", "0> INVALID_MESSAGE\n")
    test(s, "DB POST BUY RICE BLAH 1\n", "0> INVALID_MESSAGE\n")

    # POST / POST_CONFIRMATION
    test(s, "DB POST BUY RICE 100 5.3\n", "0> 0 DB BUY RICE 100 5.3 HAS BEEN POSTED\n")
    test(s, "MS POST SELL RICE 100 5.5\n", "0> 1 MS SELL RICE 100 5.5 HAS BEEN POSTED\n")

    # UNKNOWN_ORDER
    test(s, "DB CHECK 2\n", "0> UNKNOWN_ORDER\n")
    test(s, "DB REVOKE 2\n", "0> UNKNOWN_ORDER\n")
    test(s, "MS AGGRESS 2 1\n", "0> UNKNOWN_ORDER\n")
    # NEED TO RECONSIDER!!!!
    # doesn't return orders that went through, and disregards the rest
    # see test under AGGRESS that currently fails

    # CHECK / ORDER_INFO
    test(s, "DB CHECK 0\n", "0> 0 DB BUY RICE 100 5.3\n")

    # UNAUTHORIZED
    test(s, "MS CHECK 0\n", "0> UNAUTHORIZED\n")
    test(s, "MS REVOKE 0\n", "0> UNAUTHORIZED\n")

    # UNKNOWN_DEALER
    test(s, "CRAIG POST BUY RICE 100 5.3\n", "0> UNKNOWN_DEALER\n")
    test(s, "CRAIG LIST\n", "0> UNKNOWN_DEALER\n")
    test(s, "CRAIG CHECK 0\n", "0> UNKNOWN_DEALER\n")
    test(s, "CRAIG REVOKE 0\n", "0> UNKNOWN_DEALER\n")
    test(s, "CRAIG AGGRESS 0 5\n", "0> UNKNOWN_DEALER\n")
    test(s, "DB LIST RICE CRAIG\n", "0> UNKNOWN_DEALER\n")

    # UNKNOWN_COMMODITY
    test(s, "DB POST BUY CRAIG 100 5.3\n", "0> UNKNOWN_COMMODITY\n")
    test(s, "DB LIST CRAIG\n", "0> UNKNOWN_COMMODITY\n")

    # LIST / ORDER_INFO_LIST
    test(s, "CITI LIST\n", "2> 0 DB BUY RICE 100 5.3\n1> 1 MS SELL RICE 100 5.5\n0> END OF LIST\n")
    test(s, "CITI LIST RICE\n", "2> 0 DB BUY RICE 100 5.3\n1> 1 MS SELL RICE 100 5.5\n0> END OF LIST\n")
    test(s, "CITI LIST RICE DB\n", "1> 0 DB BUY RICE 100 5.3\n0> END OF LIST\n")

    # AGGRESS / TRADE_REPORT
    test(s, "CITI AGGRESS 0 5\n", "0> SOLD 5 RICE @ 5.3 FROM DB\n")
    test(s, "CITI AGGRESS 1 5\n", "0> BOUGHT 5 RICE @ 5.5 FROM MS\n")

    # AGGRESS / TRADE_REPORT_LIST
    test(s, "CITI AGGRESS 0 5 1 5\n", "2> SOLD 5 RICE @ 5.3 FROM DB\n1> BOUGHT 5 RICE @ 5.5 FROM MS\n0> END OF LIST\n")
    test(s, "CITI AGGRESS 0 5 2 5 0 5\n", "2> SOLD 5 RICE @ 5.3 FROM DB\n1> SOLD 5 RICE @ 5.3 FROM DB\n0> END OF LIST\n")


    # REVOKE / REVOKED
    test(s, "DB REVOKE 0\n", "0> 0 HAS BEEN REVOKED\n")

    # CHECK / FILLED
    test(s, "CITI AGGRESS 1 90\n", "0> BOUGHT 90 RICE @ 5.5 FROM MS\n")
    test(s, "MS CHECK 1\n", "0> 1 HAS BEEN FILLED\n")

    s.close
    puts "Summary: #{$failed}/#{$total} tests failed"
end
