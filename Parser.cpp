//
// Parser.cpp
// Author: Craig Liebmann
//
      

//#include "Command.hpp"
#include "Parser.hpp"
#include <string>
#include <queue>
#include <iostream>
#include <boost/regex.hpp>

#include <boost/lexical_cast.hpp>

// debug
#include <cassert>

using namespace std;

// public

Parser::Parser() 
  : int_re_("(\\d)+"),
    float_re_("(\\d)*\\.(\\d)*")
{}

Parser::~Parser() {}

Command* Parser::parse(queue<string> tokens) {
    // assert(tokens.size() > 1); // must contain command name
    if (tokens.size() < 2) return NULL;

    string dealer_id = tokens.front(); tokens.pop();
    string cmd_name = tokens.front(); tokens.pop();

    if (cmd_name == "POST")
        return parse_post(dealer_id, tokens);
    if (cmd_name == "REVOKE")
        return parse_revoke(dealer_id, tokens);
    if (cmd_name == "CHECK")
        return parse_check(dealer_id, tokens);
    if (cmd_name == "LIST")
        return parse_list(dealer_id, tokens);
    if (cmd_name == "AGGRESS")
        return parse_aggress(dealer_id, tokens);

    return NULL;
}


//private

Command* Parser::parse_post(string dealer_id, queue<string> tokens) {
    if (tokens.size() != 4) return NULL;


    string side = tokens.front(); tokens.pop();
    string commodity = tokens.front(); tokens.pop();
    if (!boost::regex_match(tokens.front(), int_re_))
        return NULL;
    int amount = boost::lexical_cast<int>(tokens.front()); tokens.pop();
    if (!boost::regex_match(tokens.front(), float_re_) || tokens.front() == string("."))
        return NULL;
    double price = boost::lexical_cast<double>(tokens.front()); tokens.pop();

    return new POST(dealer_id, side, commodity, amount, price);
}

//-----------------------------------------------------------------------------

Command* Parser::parse_revoke(string dealer_id, queue<string> tokens) {
    if (tokens.size() != 1) return NULL;

    if (!boost::regex_match(tokens.front(), int_re_))
        return NULL;
    int order_id = boost::lexical_cast<int>(tokens.front()); tokens.pop();

    return new REVOKE(dealer_id, order_id);
}

//-----------------------------------------------------------------------------

Command* Parser::parse_check(string dealer_id, queue<string> tokens) {
    if (tokens.size() != 1) return NULL;

    if (!boost::regex_match(tokens.front(), int_re_))
        return NULL;
    int order_id = boost::lexical_cast<int>(tokens.front()); tokens.pop();

    return new CHECK(dealer_id, order_id);
}

//-----------------------------------------------------------------------------

Command* Parser::parse_list(string dealer_id, queue<string> tokens) {
    if (tokens.size() > 2) return NULL;
    string commodity;
    string other_dealer_id;

    if (tokens.empty()) return new LIST(dealer_id);
    commodity = tokens.front(); tokens.pop();

    if (tokens.empty()) return new LIST(dealer_id, commodity);
    other_dealer_id = tokens.front(); tokens.pop();

    return new LIST(dealer_id, commodity, other_dealer_id);
}

//-----------------------------------------------------------------------------

Command* Parser::parse_aggress(string dealer_id, queue<string> tokens) {
    if (tokens.size() < 2) return NULL;
    if (tokens.size() % 2 != 0) return NULL;

    queue<aggress_pair> aggress_pairs;
    while (!tokens.empty()) {
        struct aggress_pair ap;
        if (!boost::regex_match(tokens.front(), int_re_))
            return NULL;
        ap.order_id = boost::lexical_cast<int>(tokens.front()); tokens.pop();
        if (!boost::regex_match(tokens.front(), int_re_))
            return NULL;
        ap.amount = boost::lexical_cast<int>(tokens.front()); tokens.pop();
        aggress_pairs.push(ap);
    }

    return new AGGRESS(dealer_id, aggress_pairs);
}

//-----------------------------------------------------------------------------


// int main(int argc, char* argv[]) {
//     Command* c;

//     queue<string> tokens0;
//     tokens0.push("dealer");
//     tokens0.push("POST");
//     tokens0.push("1");

//     c = Parser::parse(tokens0);
//     if (c == NULL) {
//         cout << "INVALID_MESSAGE" << endl;
//     }
//     else {
//         c->execute();
//         delete c;
//     }

//     queue<string> tokens1;
//     tokens1.push("dealer");
//     tokens1.push("REVOKE");
//     tokens1.push("1");

//     c = Parser::parse(tokens1);
//     if (c == NULL) {
//         cout << "INVALID_MESSAGE" << endl;
//     }
//     else {
//         c->execute();
//         delete c;
//     }

//     queue<string> tokens2;
//     tokens2.push("dealer");
//     tokens2.push("CHECK");
//     tokens2.push("1");

//     c = Parser::parse(tokens2);
//     if (c == NULL) {
//         cout << "INVALID_MESSAGE" << endl;
//     }
//     else {
//         c->execute();
//         delete c;
//     }

//     queue<string> tokens3;
//     tokens3.push("dealer");
//     tokens3.push("LIST");
//     tokens3.push("commodity");
//     //tokens3.push("dealer_id");

//     c = Parser::parse(tokens3);
//     if (c == NULL) {
//         cout << "INVALID_MESSAGE" << endl;
//     }
//     else {
//         c->execute();
//         delete c;
//     }

//     queue<string> tokens4;
//     tokens4.push("dealer");
//     tokens4.push("AGGRESS");
//     tokens4.push("1");

//     c = Parser::parse(tokens4);
//     if (c == NULL) {
//         cout << "INVALID_MESSAGE" << endl;
//     }
//     else {
//         c->execute();
//         delete c;
//     }

//     queue<string> tokens5;
//     tokens5.push("dealer");
//     tokens5.push("blah");
//     tokens5.push("1");

//     c = Parser::parse(tokens5);
//     if (c == NULL) {
//         cout << "INVALID_MESSAGE" << endl;
//     }
//     else {
//         c->execute();
//         delete c;
//     }

// }