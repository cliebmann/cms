//
// Command.cpp
// Author: Craig Liebmann
//
// g++ Command.cpp -o command
//

#include "Command.hpp"
#include "Message.hpp"
#include "OrderBook.hpp"
#include "MyDebug.hpp"
#include <string>
#include <queue>
#include <vector>

// Debug
#include <iostream>

class ERROR;

using namespace std;

//-----------------------------------------------------------------------------
// Should probably be someplace else. 
// Use hashes to check them??

static const char*  dealers[]= { "DB", "JPM", "UBS", "RBC", "BARX", "MS", "CITI", "BOFA", "RBS", "HSBC" };
vector<string> DEALERS(dealers, dealers+10);
static const char* commodities[] = { "GOLD", "SILV", "PORK", "OIL", "RICE" };
vector<string> COMMODITIES(commodities, commodities+5);
static const char* sides[] = { "BUY", "SELL" };
vector<string> SIDES(sides, sides+2);

bool vector_contains(vector<string> v, string s) {
    return find(v.begin(), v.end(), s) != v.end();  
}
//-----------------------------------------------------------------------------

Command::Command(std::string dealer_id)
  : dealer_id_(dealer_id) {
    DEBUG_STDOUT("Command::Command\n");
  }

Command::~Command() {
    DEBUG_STDOUT("Command::~Command\n");
}

//-----------------------------------------------------------------------------

POST::POST(string dealer_id, 
		   string side,
		   string commodity,
		   int amount, 
		   double price)
  : Command(dealer_id), 
  	side_(side),
  	commodity_(commodity),
  	amount_(amount),
  	price_(price) 
 {
    DEBUG_STDOUT("POST::POST\n"); 
}

POST::~POST() {
    DEBUG_STDOUT("POST::~POST\n");

}

Output_Message* POST::execute(OrderBook& ob) {
	DEBUG_STDOUT("POST::execute()\n");

    if (!vector_contains(DEALERS, dealer_id_))
        return new ERROR(UNKNOWN_DEALER);
    if (!vector_contains(COMMODITIES, commodity_))
        return new ERROR(UNKNOWN_COMMODITY);
    if (!vector_contains(SIDES, side_))
        return new ERROR(INVALID_MESSAGE);

    Order* order = new Order(ob.next_order_number(), *this);
    ob.post(*order);

    ORDER_INFO* oi = new ORDER_INFO(*order);
    POST_CONFIRMATION* pc = new POST_CONFIRMATION(*oi);

    delete order;
    delete oi;

    return  pc;
}

//-----------------------------------------------------------------------------

REVOKE::REVOKE(string dealer_id,
		       int order_id)
  : Command(dealer_id),
    order_id_(order_id)
{
    DEBUG_STDOUT("REVOKE::REVOKE\n");
}

REVOKE::~REVOKE() {
    DEBUG_STDOUT("REVOKE::~REVOKE\n");
}

Output_Message* REVOKE::execute(OrderBook& ob) {
	DEBUG_STDOUT("REVOKE::execute()\n");

    if (!vector_contains(DEALERS, dealer_id_))
        return new ERROR(UNKNOWN_DEALER);
    if (order_id_ >= ob.num_orders_)
        return new ERROR(UNKNOWN_ORDER);

    Order order = ob.check(order_id_);
    if (dealer_id_ != order.getDealerId())
        return new ERROR(UNAUTHORIZED);

    ob.revoke(order_id_);

    return new REVOKED(order_id_);
}

//-----------------------------------------------------------------------------

CHECK::CHECK(string dealer_id,
		     int order_id)
  : Command(dealer_id),
    order_id_(order_id)
{
    DEBUG_STDOUT("CHECK::CHECK\n");
}

CHECK::~CHECK() {
    DEBUG_STDOUT("CHECK::~CHECK\n");
}

Output_Message* CHECK::execute(OrderBook& ob) {
	DEBUG_STDOUT("CHECK::execute()\n");

    if (!vector_contains(DEALERS, dealer_id_))
        return new ERROR(UNKNOWN_DEALER);
    if (order_id_ >= ob.num_orders_)
        return new ERROR(UNKNOWN_ORDER);

    Order order = ob.check(order_id_);
    if (dealer_id_ != order.getDealerId())
        return new ERROR(UNAUTHORIZED);

    if (order.filled())
        return new FILLED(order_id_);

    return new ORDER_INFO(order);
}

//-----------------------------------------------------------------------------

LIST::LIST(string dealer_id)
  : Command(dealer_id)
{}

LIST::LIST(string dealer_id,
            string commodity)
  : Command(dealer_id),
    commodity_(commodity)
{}

LIST::LIST(string dealer_id,
 	        string commodity,
 	        string other_dealer_id)
  : Command(dealer_id),
    commodity_(commodity),
    other_dealer_id_(other_dealer_id)
{
    DEBUG_STDOUT("LIST::LIST\n");
}

LIST::~LIST() {
    DEBUG_STDOUT("LIST::~LIST\n");
}

Output_Message* LIST::execute(OrderBook& ob) {
	DEBUG_STDOUT("LIST::execute()\n");

    if (!vector_contains(DEALERS, dealer_id_))
        return new ERROR(UNKNOWN_DEALER);
    if (!commodity_.empty() && !vector_contains(COMMODITIES, commodity_))
        return new ERROR(UNKNOWN_COMMODITY);
    if (!other_dealer_id_.empty() && !vector_contains(DEALERS, other_dealer_id_))
        return new ERROR(UNKNOWN_DEALER);

    queue<Order> q = ob.list(commodity_, other_dealer_id_);

    ORDER_INFO_LIST* oil = new ORDER_INFO_LIST;
    while (!q.empty()) {
        oil->add_order(q.front());
        q.pop();
    }

    return oil;
}

//-----------------------------------------------------------------------------

AGGRESS::AGGRESS(string dealer_id,
 	             queue<aggress_pair> aggress_pairs)
  : Command(dealer_id),
    aggress_pairs_(aggress_pairs)
{
    DEBUG_STDOUT("AGGRESS::AGGRESS\n");
}

AGGRESS::~AGGRESS() {
    DEBUG_STDOUT("AGGRESS::~AGGRESS\n");
}

Output_Message* AGGRESS::execute(OrderBook& ob) {
	DEBUG_STDOUT("AGGRESS::execute()\n");

    if (!vector_contains(DEALERS, dealer_id_))
        return new ERROR(UNKNOWN_DEALER);

    TRADE_REPORT_LIST* trl = new TRADE_REPORT_LIST();

    bool only_one = aggress_pairs_.size() == 1;
    while (!aggress_pairs_.empty()) {
        struct aggress_pair ap = aggress_pairs_.front();
        aggress_pairs_.pop();
        int order_id = ap.order_id;
        int amount = ap.amount;

        // doesn't send back trades that went through, and doesn't try the rest
        if (order_id >= ob.num_orders_) {
            if (!only_one)
                continue;
            delete trl;
            return new ERROR(UNKNOWN_ORDER);
        }

        int num_filled = ob.aggress(order_id, amount);

        Order order = ob.check(order_id);
        enum Action act;
        if (order.getSide() == "BUY") act = SOLD;
        else                          act = BOUGHT;

        TRADE_REPORT* tr = new TRADE_REPORT(act, num_filled, order.getCommodity(),
                                       order.getPrice(), order.getDealerId());

        if (only_one) { delete trl; return tr; }
        trl->add_trade_report(*tr);

        delete tr;
    }

    return trl;
}

//-----------------------------------------------------------------------------





// int main(int argc, char* argv[]) {
//     string dealer("dealer");
//     CHECK* c = new CHECK(dealer, 1);
//     c->execute();
//     delete c;

//     REVOKE r = REVOKE(dealer, 2);
//     r.execute();
// }


