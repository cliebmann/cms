//
// Server.cpp
// Author: Craig Liebmann
//

#include "Server.hpp"
#include "Connection.hpp"
#include "MyDebug.hpp"
//#include "Platform.hpp"
#include <boost/asio.hpp>
#include <boost/bind.hpp>

using namespace std;
using boost::asio::ip::tcp;

Server::Server(boost::asio::io_service& io_service, const std::string& address, 
	const std::string& port, /*CmdQueue* queue,*/ Multi_Async_Platform* pm) 
  : io_service_(io_service),
    // signals_(io_service_),
    acceptor_(io_service_), //tcp::endpoint(tcp::v4(), port)),
    new_connection_()
{
    DEBUG_STDOUT("Server::Server\n");
    accepted_ = false;
    pm_ = pm;
    // queue_ = queue;

//     signals_.add(SIGINT);
//     signals_.add(SIGTERM);
// #if defined(SIGQUIT)
//     signals_.add(SIGQUIT);
// #endif // defined(SIGQUIT)
//     signals_.async_wait(boost::bind(&Server::handle_stop, this));

    tcp::resolver resolver(io_service_);
    tcp::resolver::query query(address, port);
    tcp::endpoint endpoint = *resolver.resolve(query);
    acceptor_.open(endpoint.protocol());
    acceptor_.set_option(tcp::acceptor::reuse_address(true));
    acceptor_.bind(endpoint);
    acceptor_.listen();

    start_accept();
}


Server::~Server() {
    DEBUG_STDOUT("Server::~Server\n");
}

void Server::run() {
    io_service_.run();
}

void Server::start_accept() {
    DEBUG_STDOUT("Server::start_accept\n");
    // cout << "accepted: " << accepted_ << endl;
    // cout << connections_.size() << endl;
    if (accepted_ && connections_.empty()) {
        return;
    }

    new_connection_.reset(new Connection(io_service_, pm_, this));
    acceptor_.async_accept(new_connection_->socket(),
        boost::bind(&Server::handle_accept, this,
            boost::asio::placeholders::error));
}

void Server::handle_accept(const boost::system::error_code& error) {
    if (!acceptor_.is_open()) {
        return;
    }

    if (!error) {
        start_connection(new_connection_);
    }
    accepted_ = true;
    start_accept();
}

void Server::handle_stop() {
    acceptor_.close();
    stop_all_connections();
}

void Server::start_connection(boost::shared_ptr<Connection> csp) {
    connections_.insert(csp);
    csp->start();
}

void Server::stop_connection(boost::shared_ptr<Connection> csp) {
    DEBUG_STDOUT("stop_connection\n");
    connections_.erase(csp);
    csp->stop();

    if (connections_.empty()) {
        acceptor_.close();
    }
}

void Server::stop_all_connections() {
    for_each(connections_.begin(), connections_.end(),
        boost::bind(&Connection::stop, _1));
    connections_.clear();
}