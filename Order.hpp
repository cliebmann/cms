//
// Order.hpp
// Author: Craig Liebmann
//

#ifndef ORDER_HPP
#define ORDER_HPP

#include "Command.hpp"
#include <string>

class POST;

class Order {
public:
    Order(int order_id, POST& post);
	Order(int order_id,
          std::string dealer_id,
          std::string side,
          std::string commodity,
          int amount,
          double price);
	~Order();

    void revoke();
    bool revoked() const;
    bool filled() const;
    bool open() const;
	std::string to_str() const;

    // inline Accessors
    std::string getCommodity() const { return commodity_; }    
    std::string getDealerId() const { return dealer_id_; }
    double getPrice() const { return price_; }
    std::string getSide() const { return side_; }
    int getOrderId() const { return order_id_; }

    int remaining_;

private:
	std::string dealer_id_;
	std::string side_;
	std::string commodity_;

	int amount_;
	double price_;

  int order_id_;
  bool revoked_;
};

#endif // ORDER_HPP