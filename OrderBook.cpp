//
// OrderBook.cpp
// Author: Craig Liebmann
//

#include "OrderBook.hpp"
#include "Command.hpp"
#include "MyDebug.hpp"
#include <boost/thread/mutex.hpp>
#include <string>
#include <queue>

// debug
#include <iostream>

using namespace std;

//class Order;

OrderBook::OrderBook() {
    DEBUG_STDOUT("OrderBook::OrderBook\n");
    num_orders_ = 0;
}

OrderBook::~OrderBook() {
    DEBUG_STDOUT("OrderBook::~OrderBook\n");
}


void OrderBook::post(Order& order) {
    boost::mutex::scoped_lock lock(mutex_);
    orders_.push_back(order);
}

void OrderBook::revoke(int order_id) {
    boost::mutex::scoped_lock lock(mutex_);
    orders_[order_id].revoke();
}

Order OrderBook::check(int order_id) {
    boost::mutex::scoped_lock lock(mutex_);
    return orders_[order_id];
}

queue<Order> OrderBook::list() {
    return list("", "");
}

queue<Order> OrderBook::list(string commodity) {
    return list(commodity, "");
}

queue<Order> OrderBook::list(string commodity, string dealer_id) {
    boost::mutex::scoped_lock lock(mutex_);
    queue<Order> q;
    for (int i = 0; i < orders_.size(); ++i) {
        if ( (orders_[i].open()) &&
             (commodity.empty() || orders_[i].getCommodity() == commodity) &&
             (dealer_id.empty() || orders_[i].getDealerId() == dealer_id) ) {
            q.push(orders_[i]);
        }
    }
    return q;
}

// why isn't o.remaining_ changed?
int OrderBook::aggress(int order_id, int amount) {
    boost::mutex::scoped_lock lock(mutex_);
    Order o = orders_[order_id];
    if (o.remaining_ < amount)
        amount = o.remaining_;

    // need to change actual order
    orders_[order_id].remaining_ -= amount;
    // o.remaining_ -= amount;
    return amount;
}

int OrderBook::next_order_number() {
    boost::mutex::scoped_lock lock(mutex_);
    return num_orders_++; // increment should occur after?
}