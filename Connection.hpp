//
// Connection.hpp
// Author: Craig Liebmann
//

#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
// #include "CmdQueue.hpp"
#include "Platform.hpp"
#include <string>

// class CmdQueue;
class Multi_Async_Platform;
class Server;

class Connection 
  : public boost::enable_shared_from_this<Connection>
{
public:
    Connection(boost::asio::io_service& io_service, Multi_Async_Platform* pm,
        Server* s);
    ~Connection();

    boost::asio::ip::tcp::socket& socket();

    void start();
    void stop();
    void write(const std::string str);


    void handle_write(const boost::system::error_code& e);

private:
    void handle_read(const boost::system::error_code& e,
        std::size_t bytes_transferred);


    boost::asio::ip::tcp::socket socket_;
    boost::array<char, 8192> buffer_;
    // connection_manager& connection_manager_;
    // request& request_;
    // reply reply_;

    // CmdQueue* queue_;
    Multi_Async_Platform* pm_;
    Server* s_;
};

//typedef boost::shared_ptr<Connection> connection_ptr;

#endif // CONNECTION_HPP