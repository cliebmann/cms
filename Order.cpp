//
// Order.cpp
// Author: Craig Liebmann
//

#include "Order.hpp"
//#include "Command.hpp"
#include "OrderBook.hpp"
#include "MyDebug.hpp"
#include <string>
#include <sstream>

// debug
#include <iostream>

using namespace std;

Order::Order(int order_id, POST& post) {
    DEBUG_STDOUT("Order::Order\n");

    dealer_id_ = post.getDealerId(); 
    side_ = post.getSide();
    commodity_ = post.getCommodity();
    amount_ = post.getAmount();
    price_ = post.getPrice();

    remaining_ = amount_;
    revoked_ = false;

    order_id_ = order_id;
}

Order::Order(int order_id,
             string dealer_id,
             string side,
             string commodity,
             int amount,
             double price)
  : order_id_(order_id),
    dealer_id_(dealer_id), side_(side), commodity_(commodity),
    amount_(amount), price_(price), remaining_(amount) {
    
    DEBUG_STDOUT("Order::Order\n");
    revoked_ = false;

    // dealer_id_ = post.getDealerId();
    // side_ = post.getSide();
    // commodity_ = post.getCommodity();
    // initial_amount_ = post.getAmount();
    // price_ = post.getPrice();

    // amount_ = initial_amount_;
}

Order::~Order() {
    DEBUG_STDOUT("Order::~Order\n");
}

void Order::revoke() {
    revoked_ = true;
}

bool Order::revoked() const {
    return revoked_;
}

bool Order::filled() const {
    return remaining_ == 0;
}

bool Order::open() const {
    return !revoked() && !filled();
}

string Order::to_str() const {
    stringstream ss;

    ss << order_id_ << " ";
    ss << dealer_id_ << " ";
    ss << side_ << " ";
    ss << commodity_ << " ";
    ss << remaining_ << " ";
    ss << price_;

    return ss.str();
}

