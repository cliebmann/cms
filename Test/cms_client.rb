#!/usr/bin/env ruby

require 'socket'

def prompt(*args)
    print(*args)
    gets
end

host = 'localhost'
port = 2000

s = TCPSocket.open(host, port)
  
while min= prompt('% ')
    if min == "close\n"
        break
    end

    s.write(min)
    loop {
        mout = s.readline
        print(mout)
        if 0 == mout.scan(/\d+/)[0].to_i
            break
        end
    }
end
s.close
