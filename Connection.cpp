//
// Connection.cpp
// Author: Craig Liebmann
//

#include "Connection.hpp"
#include "Message.hpp"
#include "Command.hpp"
#include "Parser.hpp"
#include "Platform.hpp"
#include "MyDebug.hpp"

#include <string>
#include <queue>

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>

using namespace std;
using boost::asio::ip::tcp;


Connection::Connection(boost::asio::io_service& io_service, 
    Multi_Async_Platform* pm, Server* s) 
  : socket_(io_service)
{
    DEBUG_STDOUT("Connection::Connection\n");
    // queue_ = queue;
    pm_ = pm;
    s_ = s;
}

Connection::~Connection() {
    DEBUG_STDOUT("Connection::~Connection\n");
}

tcp::socket& Connection::socket() {
    return socket_;
}

void Connection::start() {
    DEBUG_STDOUT("Connection::start\n");
    socket_.async_read_some(boost::asio::buffer(buffer_),
        boost::bind(&Connection::handle_read, shared_from_this(),
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void Connection::stop() {
    DEBUG_STDOUT("Connection::stop\n");
    socket_.close();
}

void Connection::handle_read(const boost::system::error_code& error,
    size_t bytes_transferred) {
    DEBUG_STDOUT("Connection::handle_read\n");


    if (!error) {
        try {
            string line(buffer_.data(), buffer_.data() + bytes_transferred);

            Input_Message* im = new Input_Message(line);
            if (!im->isValid()) {
                delete im;
                write(ERROR(INVALID_MESSAGE).to_str());
                return;
            }

            queue<string> tokens = im->tokenize();
            delete im;

            Parser p;
            Command* cmd = p.parse(tokens);
            if (!cmd) {
                write(ERROR(INVALID_MESSAGE).to_str());
                return;
            }

            // struct cmd_conn_pair ccp;
            // ccp.cmd_p = cmd;
            // ccp.conn_sp = boost::shared_ptr<Connection>(this);
            // queue_->push(ccp);

            // boost::mutex::scoped_lock lock(pm_->mutex_);
            Output_Message* om = cmd->execute(*(pm_->ob_));
            delete cmd;

            string out = om->to_str();
            delete om;
            write(out);
        }
        catch (std::exception& e)
        {
            cerr << "EXCEPTION: " << e.what() << endl;
        }
    }
    else if (error != boost::asio::error::operation_aborted) {
        s_->stop_connection(shared_from_this());
    }
    else {
        DEBUG_STDOUT("error in Connection::handle_read\n");
    }
}

void Connection::handle_write(const boost::system::error_code& error) {
    DEBUG_STDOUT("Connection::handle_write\n");
    if (!error) {
        socket_.async_read_some(boost::asio::buffer(buffer_),
            boost::bind(&Connection::handle_read, shared_from_this(),
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred));
    }
    else if (error != boost::asio::error::operation_aborted) {
        s_->stop_connection(shared_from_this());
    }
    else {
        DEBUG_STDOUT("error in Connection:handle_write\n");
    }
}

void Connection::write(const string str) {
    DEBUG_STDOUT("Connection::write\n");
    stringstream ss;
    ss << str <<endl;
    string s = ss.str();


    boost::asio::async_write(socket_,
        boost::asio::buffer(s),
        boost::bind(&Connection::handle_write, shared_from_this(),
            boost::asio::placeholders::error));
}

