//
// Message.cpp
// Author: Craig Liebmann
//
// g++ Command.cpp Parser.cpp Order.cpp Message.cpp -o message
//

#include "Message.hpp"
#include "MyDebug.hpp"
#include <string>
#include <queue>
#include <sstream>
#include <iostream>


// Debug
#include <cassert>
//#include "Command.hpp"
//#include "Parser.hpp"
//#include "Order.hpp"

using namespace std;

// public

Input_Message::Input_Message(string line)
  : content_(line) 
{
    valid_ = testValidity();
    DEBUG_STDOUT("Input_Message::Input_Message\n");
}

Input_Message::~Input_Message() {
    DEBUG_STDOUT("Input_Message::~Input_Message\n");

}

bool Input_Message::isValid() {
    return valid_;
}

// delimeters argument?
queue<string> Input_Message::tokenize() {
    assert(isValid());

    string s;       
    queue<string> tokens;

    istringstream isstream(content_, istringstream::in);

    while (isstream >> s) {
        tokens.push(s);
    }

    return tokens;
}

// private

bool Input_Message::testValidity() {
    return ( content_.length() <= Input_Message::max_length ) &&
           ( !content_.empty() );
}




//-----------------------------------------------------------------------------

Output_Message::Output_Message() {
    DEBUG_STDOUT("Output_Message::Output_Message\n");
}
Output_Message::~Output_Message() {
    DEBUG_STDOUT("Output_Message::~Output_Message\n");
}

// currently doesn't work
ostream& operator<<(ostream& os, const Output_Message& om) {
    os << om.to_str();
    return os;
}

//-----------------------------------------------------------------------------

// automagic copy-constructor ?
ORDER_INFO::ORDER_INFO(Order& order) 
  : order_(order)
{
    DEBUG_STDOUT("ORDER_INFO::ORDER_INFO\n");
    // int order_id_ = order.getOrderId();
    // dealer_id_ = order.getDealerId();
    // side_ = order.getSide();
    // commodity_ = order.getCommodity();
    // remaining_amount_ = order.remaining_;
    // price_ = order.getPrice();
}

ORDER_INFO::~ORDER_INFO() {
    //delete order_;
    DEBUG_STDOUT("ORDER_INFO::~ORDER_INFO\n");
}

string ORDER_INFO::to_str() const {
    stringstream ss;

    ss << "0> " << to_str_plain();
    // ss << "> ";
    // ss << order_id_ << " ";
    // ss << dealer_id_ << " ";
    // ss << side_ << " ";
    // ss << commodity_ << " ";
    // ss << remaining_amount_ << " ";
    // ss << price_ << " ";

    return ss.str();
}

// without leading >
string ORDER_INFO::to_str_plain() const {
    return order_.to_str();
}

//-----------------------------------------------------------------------------

// need to make new?
ORDER_INFO_LIST::ORDER_INFO_LIST() {
    DEBUG_STDOUT("ORDER_INFO_LIST::ORDER_INFO_LIST\n");
    //orders_ = new vector<Order>;
}

ORDER_INFO_LIST::~ORDER_INFO_LIST() {
    DEBUG_STDOUT("ORDER_INFO_LIST::~ORDER_INFO_LIST\n");
    //delete orders_;
}

void ORDER_INFO_LIST::add_order(Order& order) {
    orders_.push_back(order);
}

string ORDER_INFO_LIST::to_str() const {
    stringstream ss;

    for (int i = 0; i < orders_.size(); i++) {
        int left = orders_.size() - i;
        ss << left << "> " << orders_[i].to_str_plain() << endl;
    }
    ss << "0> END OF LIST";

    return ss.str();
}

//-----------------------------------------------------------------------------

TRADE_REPORT::TRADE_REPORT(enum Action act, int amount, 
                          string commodity, double price, string dealer_id) 
  : amount_(amount), commodity_(commodity),
    price_(price), dealer_id_(dealer_id)
{
    DEBUG_STDOUT("TRADE_REPORT::TRADE_REPORT\n");
    // should location of switch be uniform?
    switch (act) {
        case BOUGHT:
            action_ = "BOUGHT";
            break;
        case SOLD:
            action_ = "SOLD";
            break;
        default:
            action_ = "ERROR";
    }
}

TRADE_REPORT::~TRADE_REPORT() {
    DEBUG_STDOUT("TRADE_REPORT::~TRADE_REPORT\n");
}

string TRADE_REPORT::to_str_plain() const {
    stringstream ss;

    // ss << "> ";
    ss << action_ << " ";
    ss << amount_ << " ";
    ss << commodity_ << " ";
    ss << "@ ";
    ss << price_ << " ";
    ss << "FROM ";
    ss << dealer_id_;

    return ss.str();
}

string TRADE_REPORT::to_str() const {
    stringstream ss;

    ss << "0> ";
    ss << to_str_plain();

    return ss.str();
}

//-----------------------------------------------------------------------------


TRADE_REPORT_LIST::TRADE_REPORT_LIST() {
    DEBUG_STDOUT("TRADE_REPORT_LIST::TRADE_REPORT_LIST\n");
}
TRADE_REPORT_LIST::~TRADE_REPORT_LIST() {
    DEBUG_STDOUT("TRADE_REPORT_LIST::~TRADE_REPORT_LIST\n");
}

void TRADE_REPORT_LIST::add_trade_report(TRADE_REPORT& tr) {
    trades_.push_back(tr);
}

string TRADE_REPORT_LIST::to_str() const {
    stringstream ss;

    for (int i = 0; i < trades_.size(); i++) {
        int left = trades_.size() - i;
        ss << left << "> " << trades_[i].to_str_plain() << endl;
    }
    ss << "0> END OF LIST";

    return ss.str();
}

//-----------------------------------------------------------------------------

FILLED::FILLED(int order_id)
  : order_id_(order_id) {
    DEBUG_STDOUT("FILLED::FILLED\n");
  }

FILLED::~FILLED() {
    DEBUG_STDOUT("FILLED::~FILLED\n");
}

string FILLED::to_str() const {
    stringstream ss;
    ss << "0> "<< order_id_ << " HAS BEEN FILLED";
    return ss.str();
}

//-----------------------------------------------------------------------------

REVOKED::REVOKED(int order_id) 
  : order_id_(order_id) {
    DEBUG_STDOUT("REVOKED::REVOKED\n");
  }

REVOKED::~REVOKED() {
    DEBUG_STDOUT("REVOKED::~REVOKED\n");
}

string REVOKED::to_str() const {
    stringstream ss;
    ss << "0> " << order_id_ << " HAS BEEN REVOKED";
    return ss.str();
}

//-----------------------------------------------------------------------------

// automagic copy-constructor ?
// Order only has strings and primitives
POST_CONFIRMATION::POST_CONFIRMATION(ORDER_INFO& oi) 
    : oi_(oi) {
        DEBUG_STDOUT("POST_CONFIRMATION::POST_CONFIRMATION\n");
    }

POST_CONFIRMATION::~POST_CONFIRMATION() {
    DEBUG_STDOUT("POST_CONFIRMATION::~POST_CONFIRMATION\n");
}

string POST_CONFIRMATION::to_str() const {
    stringstream ss;
    ss << "0> " << oi_.to_str_plain() << " HAS BEEN POSTED";
    return ss.str();
}

//-----------------------------------------------------------------------------

ERROR::ERROR(enum Error_Type et) {
    DEBUG_STDOUT("ERROR::ERROR\n");
    et_ = et;
}

ERROR::~ERROR() {
    DEBUG_STDOUT("ERROR::~ERROR\n");
}

string ERROR::to_str() const {
    stringstream ss;
    ss <<  "0> ";
    switch (et_) {
        case UNAUTHORIZED:
            ss << "UNAUTHORIZED";
            break;
        case UNKNOWN_ORDER:
            ss << "UNKNOWN_ORDER";
            break;
        case UNKNOWN_DEALER:
            ss << "UNKNOWN_DEALER";
            break;
        case UNKNOWN_COMMODITY:
            ss << "UNKNOWN_COMMODITY";
            break;
        case INVALID_MESSAGE:
            ss << "INVALID_MESSAGE";
            break;
        default:
            ss << "ERROR";
    }

    return ss.str();
}


//-----------------------------------------------------------------------------
// Output_Message

// int main() {

//     Output_Message* om0 = new FILLED(1);
//     cout << om0->to_str() << endl;
//     cout << om0 << endl; // doesn't work

//     Output_Message* om1 = new REVOKED(1);
//     cout << om1->to_str() << endl;

//     queue<string> tokens0;
//     tokens0.push("DB");
//     tokens0.push("POST");
//     tokens0.push("SELL");
//     tokens0.push("OIL");
//     tokens0.push("3");
//     tokens0.push("5.0");
//     Command* cmd = Parser::parse(tokens0);
//     if (!cmd) { cout << "INVALID_MESSAGE" << endl; return 0; }
//     POST* p = dynamic_cast<POST*>(cmd);
//     Order* order = new Order(*p);

//     ORDER_INFO* oi = new ORDER_INFO(*order);
//     Output_Message* om2 = oi;
//     cout << "ORDER_INFO:" << endl;
//     cout << om2->to_str() << endl;


//     Output_Message* om3 = new POST_CONFIRMATION(*oi);
//     cout << om3->to_str() << endl;

//     queue<string> tokens1;
//     tokens1.push("MS");
//     tokens1.push("POST");
//     tokens1.push("BUY");
//     tokens1.push("OIL");
//     tokens1.push("2");
//     tokens1.push("5.0");
//     Command* cmd2 = Parser::parse(tokens1);
//     if (!cmd2) { cout << "INVALID_MESSAGE" << endl; return 0; }
//     POST* p2 = dynamic_cast<POST*>(cmd2);
//     Order* order2 = new Order(*p2);

//     ORDER_INFO_LIST* oil = new ORDER_INFO_LIST();
//     oil->add_order(*order);
//     oil->add_order(*order2);
//     Output_Message* om4 = oil;
//     cout << "ORDER_INFO_LIST:" << endl;
//     cout << om4->to_str() << endl;

//     Output_Message* om5 = new TRADE_REPORT(BOUGHT, 10, "RICE", 
//                                            3.10, "HSBC");
//     cout << om5->to_str() << endl;

//     Output_Message* om6 = new ERROR(UNKNOWN_COMMODITY);
//     cout << om6->to_str() << endl;
// }

//-----------------------------------------------------------------------------
// Input_Message

// int main() {
//  string s1("Hello, world");
//  Input_Message m1(s1);

//  if (m1.isValid()) {
//      cout << "m1 is valid. length = " << s1.length() << endl;
//      vector<string> v = m1.tokenize();

//      for (int i=0; i < v.size(); i++) {
//          cout << v[i] << endl;
//      }
//  }

//  string s3("weird  delim\titers\n\t\thello");
//  Input_Message m3(s3);
//  if (m3.isValid()) {
//      cout << "m3 is valid. length = " << s3.length() << endl;
//      vector<string> v = m3.tokenize();

//      for (int i = 0; i < v.size(); i++) {
//          cout << v[i] << endl;
//      }
//  }

//  string s2("a very long string a very long string a very long string a very long string a very long string a very long string a very long sftring a very long string a very long string a very long string a very long string a very long string a very long string a very long string");
//  Input_Message m2(s2);

//  if (!m2.isValid()) {
//      cout << "m2 is not valid. length = " << s2.length() << endl;
//      m2.tokenize();
//  }
// }