//
// Parser.hpp
// Author: Craig Liebmann
//

#ifndef PARSER_HPP
#define PARSER_HPP

#include "Command.hpp"
#include <string>
#include <queue>
#include <boost/regex.hpp>

// caller owns the memory of the Command pointed to
// NULL indicates error => INVALID_MESSAGE
class Parser {
public:
	Parser();
	~Parser();

    Command* parse(std::queue<std::string> tokens);

private:
    Command* parse_post(std::string dealer_id, std::queue<std::string> tokens);
    Command* parse_revoke(std::string dealer_id, std::queue<std::string> tokens);
    Command* parse_check(std::string dealer_id, std::queue<std::string> tokens);
    Command* parse_list(std::string dealer_id, std::queue<std::string> tokens);
    Command* parse_aggress(std::string dealer_id, std::queue<std::string> tokens);

    const boost::regex int_re_;
    const boost::regex float_re_;
};

#endif // PARSER_HPP