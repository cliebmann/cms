//
// MyDebug.hpp
// Author: Craig Liebmann
//

#include <iostream>

// #define DEBUG

#ifdef DEBUG
#define DEBUG_STDOUT(X) (std::cout << (X));
#else
#define DEBUG_STDOUT(X)	
#endif