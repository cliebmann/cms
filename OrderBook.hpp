//
// OrderBook.hpp
// Author: Craig Liebmann
//

#ifndef ORDERBOOK_HPP
#define ORDERBOOK_HPP

#include "Order.hpp"
#include <boost/thread/mutex.hpp>
#include <vector>
#include <queue>

class Order;

class OrderBook {
public:
    OrderBook();
    ~OrderBook();

    void post(Order& order);
    Order check(int order_id);
    void revoke(int order_id);
    std::queue<Order> list();
    std::queue<Order> list(std::string commodity);
    std::queue<Order> list(std::string commodity, std::string dealer_id);
    int aggress(int order_id, int amount);

    int next_order_number();

    boost::mutex mutex_;
    int num_orders_;

private:
    std::vector<Order> orders_;
};

#endif // ORDERBOOK_HPP