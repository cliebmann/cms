//
// cms.cpp
// Author: Craig Liebmann
//

#include "Platform.hpp"
#include <string>
#include <queue>
#include <iostream>

using namespace std;



int main(int argc, char* argv[]) {
    Platform* pm;
    boost::asio::io_service io_service;

    if (argc > 1 && argv[1] == string("base")) {
        pm = new Base_Platform();
    }
    else if (argc > 2 && argv[1] == string("ext1")) {
        pm = new TCP_Platform(io_service, atoi(argv[2]));
    }
    else if (argc > 2 && argv[1] == string("async")) {
        pm = new Async_Platform(io_service, atoi(argv[2]));
    }
    else if (argc > 2 && argv[1] == string("ext2")) {
        pm = new Multi_Async_Platform(io_service, string("localhost"), string(argv[2]));
    }
    else {
        cout << "./cms [base | ext1 | ext2] <port>" << endl;
    }

    pm->run();

    delete pm;
    return 0;
}

